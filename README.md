# Pull All Repositories

This project was created for PoudlardRP by [Diremond](http://www.diremond.fr), to allow developers to pull their Git repositories.

## Getting Started

### Prerequisites

You must have a folder in which different Git repositories are instantiated, please put the Bash script in this one.

### Installing

To use the script, copy and paste it into the current folder thanks to a Git code for example. Don't forget to modify the data file to specify the referenced folders, and the branches to pull.

```
git clone https://gitlab.com/Diremond/PullAllGitRepositories.git
```

## Authors

* **Diremond** - *Java Developer* - [WebSite](http://www.diremond.fr)
