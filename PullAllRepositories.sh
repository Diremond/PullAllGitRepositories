#!/bin/bash
for path in `cat data | grep path | cut -d : -f 2`; do
  cd $path
  echo -e "\033[31mEtude du dossier : \033[34m"$path "\033[0m"
  for folder in `ls`; do
    # echo 'Etude du dossier : '$folder
    if [ -d "./$folder" ]; then
      # echo $folder 'est un dossier'
      cd $folder
      if [ -e ".git" ]; then
        echo -e "\033[32mRécupération du dossier" $folder "\033[0m"
        for branch in `cat ../../data | grep branch | cut -d : -f 2`; do
          echo -e "\033[36mPull de la branche "$branch "\033[0m"
          {
            git checkout $branch
            git pull origin $branch
          } #&>/dev/null
        done
      fi
      cd ..
    fi
  done
done
